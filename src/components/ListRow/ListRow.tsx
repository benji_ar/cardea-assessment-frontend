import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { AllLists } from "../../queries/__generated__/AllLists";
import { ALL_LISTS_QUERY } from "../../queries";
import { ListCard } from "../ListCard";
import { ListContext } from "../../context";

const Lists = () => {
  const { loading, error, data } = useQuery<AllLists>(ALL_LISTS_QUERY, {});

  console.log(data);

  return (
    <>
      {loading && <h1>Loading</h1>}
      {error && <h1>Error</h1>}
      {data && (
        <div className="mb-8">
          <p className="pb-2 m-4 text-2xl font-bold border-b border-gray-200">
            Lists
          </p>
          <div className="flex flex-col flex-wrap items-center md:flex md:flex-row ">
            {data.allLists?.map(
              (list, i) =>
                list && (
                  <ListCard
                    id={parseInt(list.id)}
                    name={list?.name}
                    url={list.image}
                    description={list.description}
                  />
                )
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default Lists;
