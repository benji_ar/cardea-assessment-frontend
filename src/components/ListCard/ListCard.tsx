import React from "react";
import { useQuery } from "react-apollo";
import { ListContext } from "../../context";
import { LIST_BY_ID_QUERY } from "../../queries/lists/listById";
import { ListById } from "../../queries/lists/__generated__/ListById";

interface ListCardProps {
  name: string;
  description: string;
  url: string;
  id: number;
}

export const ListCard: React.FC<ListCardProps> = ({
  id,
  name,
  url,
  description,
}) => {
  const { addList, removeList } = React.useContext(ListContext);

  const [selected, setSelected] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const { data, error, loading } = useQuery<ListById>(LIST_BY_ID_QUERY, {
    variables: {
      id,
    },
  });

  const toggleOpen = () => {
    setOpen((p) => !p);
  };

  const toggleSelection = () => {
    selected ? removeList(id) : addList(id);
    setSelected((p) => !p);
  };

  return (
    <div className={"mb-2 md:inline-block " + (open ? "first:w-full" : "")}>
      {open && (
        <div className="relative">
          <div
            className={
              "flex flex-col mx-4 border border-gray-200 rounded cursor-pointer md:flex-row" +
              (selected ? " bg-gray-200 " : " bg-white")
            }
            onClick={() => toggleSelection()}
          >
            <img className=" md:max-h-50" src={url} alt="List" />
            <button
              className="absolute p-0 leading-4 text-center align-middle rounded-full right-4 top-2 "
              onClick={(e) => {
                e.stopPropagation();
                toggleOpen();
              }}
            >
              X
            </button>
            <div className="m-2 md:mx-6">
              <h3 className="my-2 font-bold text-md">{name}</h3>
              <p className="mb-2 text-sm ">{description}</p>
              <h3 className="my-2 font-bold text-md">Job Preview</h3>
              {error && <div>error</div>}
              {data?.listById && (
                <div className="p-2 bg-gray-200 rounded">
                  <div className="font-medium text-md">
                    {data.listById.jobSet[0].jobTitle}
                  </div>
                  <div className="text-sm italic">
                    {data.listById.jobSet[0].company}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      )}{" "}
      {!open && (
        <div
          className={
            "max-w-xs mx-4 overflow-hidden border border-gray-200 rounded w-60 cursor-pointer " +
            (selected ? " bg-gray-200 " : " bg-white")
          }
          onClick={() => toggleSelection()}
        >
          <div className="relative">
            <img className="w-full" src={url} alt="List" />
            <button
              className="absolute px-2 pb-2 text-xl leading-4 text-center bg-black rounded-full align-center right-4 top-2 bg-opacity-20"
              onClick={(e) => {
                e.stopPropagation();
                toggleOpen();
              }}
            >
              ...
            </button>
          </div>
          <h3 className="px-2 my-2 font-bold text-md">{name}</h3>
          <p className="px-2 mb-2 overflow-hidden text-sm overflow-ellipsis whitespace-nowrap">
            {description}
          </p>
        </div>
      )}
    </div>
  );
};
