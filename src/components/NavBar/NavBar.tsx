import React from "react";

const NavBar = () => {
  return (
    <nav className="mt-4 mb-8">
      <p className="px-10 pb-4 font-serif text-2xl font-bold text-red-400 border-b border-gray-200">
        cardea-assesment
      </p>
    </nav>
  );
};

export default NavBar;
