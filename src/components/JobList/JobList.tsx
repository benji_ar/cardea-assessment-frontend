import React, { useContext } from "react";
import { useQuery } from "react-apollo";
import { ListContext } from "../../context";
import { JOBS_BY_LIST_QUERY } from "../../queries/jobs/jobsByList";
import { JobsByList } from "../../queries/jobs/__generated__/JobsByList";
import { JobCard } from "./JobCard";

const JobList = () => {
  const { selectedLists } = useContext(ListContext);

  const { loading, error, data } = useQuery<JobsByList>(JOBS_BY_LIST_QUERY, {
    variables: { ids: selectedLists },
  });

  return (
    <>
      {loading && <div>loading...</div>}
      {error && <div>error...</div>}
      <div className="pb-2 m-4 text-2xl font-bold border-b border-gray-200">
        Jobs
      </div>
      {data?.jobsByList?.length !== 0 && (
        <>
          <div className="w-full px-4 mx-auto">
            {data?.jobsByList?.map(
              (job, i) =>
                job && (
                  <JobCard
                    key={i}
                    title={job.jobTitle}
                    company={job.company}
                    location={job.location}
                    applyLink={job.applyLink}
                    salaryMax={job.maxSalary}
                    salaryMin={job.minSalary}
                    listNames={job.lists}
                  />
                )
            )}
          </div>
        </>
      )}
      {data?.jobsByList?.length === 0 && (
        <div className="mx-4 text-md ">
          Select a list above to see the jobs in a list 🤓!
        </div>
      )}
    </>
  );
};

export default JobList;
