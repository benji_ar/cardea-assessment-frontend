import React, { useState } from "react";
import { JobsByList_jobsByList_lists } from "../../../queries/jobs/__generated__/JobsByList";

interface JobCardProps {
  title: string;
  company: string;
  location: string;
  salaryMin: number;
  salaryMax: number;
  applyLink: string;
  listNames: JobsByList_jobsByList_lists[];
}

const JobCard: React.FC<JobCardProps> = ({
  title,
  company,
  location,
  salaryMax,
  salaryMin,
  applyLink,
  listNames,
}) => {
  const [open, setOpen] = useState(false);

  return (
    <div className="flex flex-col p-4 mx-auto my-4 border border-gray-200 rounded">
      <div className="flex justify-between">
        <div className="text-lg font-bold">{title}</div>
        <div className="italic">{location}</div>
      </div>
      <div className="font-light text-blue-700 text-md">{company}</div>
      {!open && (
        <div
          className="mt-2 text-sm text-gray-600 cursor-pointer"
          onClick={() => setOpen(true)}
        >
          See salary and link to apply 👇
        </div>
      )}
      {open && (
        <>
          <div className="mt-4">{`$${salaryMin}-$${salaryMax}`}</div>
          <div className="flex items-center justify-between mt-4 text-xs md:text-base">
            <div>
              {listNames.map((listName, i) => (
                <span key={i} className="p-2 bg-gray-200 rounded-full md:mr-4 ">
                  {listName.name}
                </span>
              ))}
            </div>
            <a className="p-2 mt-2 bg-red-400 rounded-lg" href={applyLink}>
              {" "}
              Apply here!
            </a>
          </div>
          <div
            className="ml-2 text-sm text-gray-600"
            onClick={() => setOpen(false)}
          >
            close
          </div>
        </>
      )}
    </div>
  );
};

export default JobCard;
