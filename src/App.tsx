import React from "react";
import { JobList } from "./components/JobList";
import Lists from "./components/ListRow/ListRow";
import { NavBar } from "./components/NavBar";
import { ListContext, useListContextValue } from "./context";

function App() {
  const listContextValue = useListContextValue();
  return (
    <ListContext.Provider value={listContextValue}>
      <div className="App">
        <NavBar />
        <div className="max-w-4xl m-auto">
          <Lists />
          <JobList />
        </div>
        <footer className="w-1/2 mx-auto mt-10 text-center text-gray-500">
          made with ❤️ by{" "}
          <a className="underline" href="https://benji.ar">
            benji.ar
          </a>
        </footer>
      </div>
    </ListContext.Provider>
  );
}

export default App;
