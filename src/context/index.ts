import React from "react";

export interface ListsContextData {
  selectedLists: number[];
  addList: (listId: number) => void;
  removeList: (listID: number) => void;
}

export const listsContextDefaultValue: ListsContextData = {
  selectedLists: [],
  addList: () => null,
  removeList: () => null,
};

export const ListContext = React.createContext<ListsContextData>(
  listsContextDefaultValue
);

export const useListContextValue = (): ListsContextData => {
  const [selectedLists, setSelectedLists] = React.useState<number[]>([]);

  const addList = (listID: number) => {
    setSelectedLists((old) => [...old, listID]);
  };

  const removeList = (listID: number) => {
    const index = selectedLists.indexOf(listID);
    const temp = [...selectedLists];
    if (index > -1) {
      temp.splice(index, 1);
      console.log(`removing ${index}`);
    }
    setSelectedLists(temp);
  };

  return {
    selectedLists,
    addList,
    removeList,
  };
};
