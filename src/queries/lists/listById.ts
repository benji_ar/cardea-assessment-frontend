import gql from "graphql-tag";

export const LIST_BY_ID_QUERY = gql`
  query ListById($id: Int!) {
    listById(id: $id) {
      id
      name
      description
      image
      listCurator
      jobSet {
        jobTitle
        company
      }
    }
  }
`;
