import gql from "graphql-tag";

export const LIST_QUERY = gql`
  query Lists($ids: [Int!]!) {
    lists(ids: $ids) {
      id
      name
      description
      image
    }
  }
`;
