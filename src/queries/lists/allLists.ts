import gql from "graphql-tag";

export const ALL_LISTS_QUERY = gql`
  query AllLists {
    allLists {
      id
      name
      description
      image
    }
  }
`;
