import gql from "graphql-tag";

export const JOBS_BY_LIST_QUERY = gql`
  query JobsByList($ids: [Int!]!) {
    jobsByList(ids: $ids) {
      id
      jobTitle
      company
      location
      minSalary
      maxSalary
      applyLink
      lists {
        name
      }
    }
  }
`;
